/*******************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:tree
 * @file 
 *
 *
 *
 * Author       - Dhruv N Shah
 *
 *******************************************************************************
 *
 * History
 *
 * Apr/15/2018, Dhruv N
 *
 * Apr/15/2018, Dhruv N, Created (description)
 *
 ******************************************************************************/
/*******************
 * Includes
 *******************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
/*************************
 * Defines
 *************************/
#define max 50

/*************************
 * Structure
 *************************/
//Binary tree declaration
struct BinaryNode{
	int data;                          //Contain value of particular leaf
	struct BinaryNode *left;           //Tree left child
	struct BinaryNode *right;          //Tree right child
};
//Calculate the execution time for particular process
struct timeval1{
	time_t tv_sec;           /* seconds */
	suseconds_t tv_usec;    /* microseconds */
};

/***********************
 * Global Variables
 ***********************/
struct timeval1 st, et;             //Start time, end time
int number;                         //Number of leaf is added by user
typedef struct BinaryNode tree;     //Declare the tree as BinaryNode data type
tree *root;                         //Pointing to first node of tree

/************************
 * Function Prototypes
 ************************/

/* Detailed description of the function.
 * This function allocate the memory for new block
 * @data is input from user where the user add new data
 * @return : New node
 */
static tree *GetNode(int data)
{
	tree *newnode = (tree *)malloc(sizeof(tree));
	newnode -> data = data;                      //user entered data is added into new node data
	newnode -> right = NULL;                     //right address of leaf is NULL
	newnode -> left = NULL;                      //left address of leaf is NULL
	return newnode;                              //Return the address of new node
}

/* Detailed description of the function.
 * This function add the new leaf at left side or right side
 * into tree depending on value of data which is entered by the user.
 * @root is input which is the head node of tree
 * @data which is input entered by the user
 * @return : Return the root node which having the new node
 */
static tree *Insert(tree *root, int data)
{
	/*In recursion if my left address or right address is NULL
	  then add new node*/
	if(root == NULL)
	{
		root = GetNode(data);
	}
	/*If user entered data is less than the root at that time
	  add the new node to left side */
	else if(root -> data >= data)
	{
		root -> left = Insert(root -> left, data);
	}
	/*If user entered data is greater than the root at that time
	  add the new node to right side */
	else
	{
		root -> right = Insert(root -> right, data);
	}

	return root;
}

/* Detailed description of the function.
 * This function find the leaf at left side or right side
 * into tree depending on value of data which is entered by the user.
 * @root is input which is the head node of tree
 * @data which is input entered by the user to find the data into tree
 * @return : Return the root node which having that particular values
 */

static tree *Find(tree *root, int data)
{

	/*In recursion if my left address or right address is NULL
	  then return NULL value which indicate that there is no node */
	if(root == NULL)
	{
		return (NULL);
	}
	/*In recursion if current value is lesser than root data
	  at that time we have to find at left sub tree */
	else if((root -> data) > data)
	{
		return Find((root -> left), data);
	}

	/*In recursion if current value is greater than root data
	  at that time we have to find at right sub tree */
	else if((root -> data < data))
	{
		return Find((root -> right), data);
	}

	/*In recursion if current value is equal to root data
	  at that time that node is successfully found*/
	else
	{
		return root;
	}
}

/* Detailed description of the function.
 * This function print the tree in preorder where we print the data
 * of parent node then left child data then right child data
 * @root is input which is the head node of tree
 */

static void preorder(tree *root)
{
	if(root != NULL)
	{
		printf(" %d ",root -> data);
		preorder(root -> left);
		preorder(root -> right);
	}
}

/* Detailed description of the function.
 * This function print the tree in inorder where we print the data
 * of left child then parent node data then right child data
 * @root is input which is the head node of tree
 */
static void inorder(tree *root)
{
	if(root != NULL)
	{
		inorder(root -> left);
		printf(" %d ", root -> data);
		inorder(root -> right);
	}
}

/* Detailed description of the function.
 * This function print the tree in postorder where we print the data
 * of left child then right child data then parent node data
 * @root is input which is the head node of tree
 */
static void postorder(tree *root)
{
	if(root != NULL)
	{
		postorder(root -> left);
		postorder(root -> right);
		printf(" %d ", root -> data);
	}
}

/* Detailed description of the function.
 * This function print the tree in level where we print the data
 * according to level of tree
 * @root is input which is the head node of tree
 * @level is input which is the level of tree's leaf
 */
void sort_level(tree *root, int level)
{
	/*In recursive function if root is NULL
	  then return to parent nod */
	if(root == NULL)
	{
		return;
	}
	/*In recursive function print the order of child
	  at level 1*/
	if(level == 1)
	{
		printf(" %d ", root -> data);
		return;
	}

	/*In recursive function print the order of child
	  at left side and then right side*/
	else if(level > 1)
	{
		sort_level(root -> left, level - 1);
		sort_level(root -> right, level - 1);
	}
}


/* Detailed description of the function.
 * This function print the tree in level where we print the data
 * according to level of tree
 * @root is input which is the head node of tree
 */
static void level_order(tree *root)
{
	int level = 0;                  //The value of level is 0
	int i = 0;                      //i is for itration
	gettimeofday(&st, NULL);
	for(i = 1 ; i <= number ; i++)
	{
		level = i;
		sort_level(root, level);
	}
	gettimeofday(&et, NULL);
	int elapsed = ((et.tv_sec - st.tv_sec) * 1000000) + (et.tv_usec - st.tv_usec);
	printf("\nSorting time: %d micro seconds\n", elapsed);
}

int main()
{
	int data;                      //user store the data
	int find_number;               //store the finding number
	char chose_flag;               //Chose the flag weather user want to add the node in binary tree
	do
	{
		printf("\nEnter %d number",number + 1);
		scanf("%d", &data);
		root = Insert(root, data);
		number++;
		printf("\nWould you like to add new number(y/n)");
		scanf(" %c", &chose_flag);

	}while((chose_flag == 'y') && (chose_flag != 'n'));

	printf("\nPreorder :-> (root left right):");
	preorder(root);
	printf("\nInorder :-> (left root right):");
	inorder(root);
	printf("\nPostorder :-> (left right root):");
	postorder(root);
	printf("\nSequence in local order is:");
	level_order(root);
	printf("\nEnter the the number to be found in tree:");
	scanf("%d",&find_number);
	root = Find(root, find_number);
	if(root == NULL)
	{
		printf("%d element is not found in list\n",find_number);
	}
	else
	{
		printf("%d element is found in the tree\n",find_number);
	}

	return 0;
}


