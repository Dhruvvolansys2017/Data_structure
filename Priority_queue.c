/*******************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:Priority queue
 * @file
 *
 *
 *
 * Author       - Dhruv N Shah
 *
 *******************************************************************************
 *
 * History
 *
 * Apr/15/2018, Dhruv N
 *
 * Apr/15/2018, Dhruv N, Created (description)
 *
 ******************************************************************************/
/*******************
 * Includes
 *******************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>


/*************************
 * Structure
 *************************/
//Node declaration
struct Node{
	int number;                 //Contain particular node number
	int priority;               //Contain the value of priority
	struct Node *next;          //Having the address of next node
};

/***********************
 * Global Variables
 ***********************/
typedef struct Node nod;       //Make one user define function of structure Node
struct timeval t0;
struct timeval t1;
float elapsed;                 //Store the value of execution time

/* Detailed description of the function.
 * This function initialize the linked list
 * @head is input which is the head node of linked list
 * @max_size_of_queue which contains the value of max size of stack or queue
 * @flag contain the value weather list is stack or queue
 * @return : Return the root node which having the new node
 */
static void Initlinklist(nod **head, int max_size_of_queue, int flag)
{
	nod *ptr;               //Temporary pointer having type of nod
	int loop = 0;           //Used for itrection
	if(flag == 0)                                             //QUEUE
	{
		for(loop = 0 ; loop < max_size_of_queue ; loop++)
		{
			if(loop == 0)
			{
				*head = (nod *)malloc(sizeof(nod));
				ptr = *head;
			}
			else
			{
				ptr -> next = (nod *)malloc(sizeof(nod));
				ptr = ptr -> next;
			}
			printf("\nEnter the %d data:",loop);
			scanf("%d", &ptr->number);
			printf("\nEnter the priority:");
			scanf("%d", &ptr -> priority);
		}
		ptr->next = NULL;
	}
	else                     				 //STACK
	{

		for(loop = 0 ; loop < max_size_of_queue ; loop++)
		{
			ptr = (nod *)malloc(sizeof(nod));
			ptr -> next = *head;
			*head = ptr;
			printf("\nEnter the %d data:",loop);
			scanf("%d", &ptr->number);
			printf("\nEnter the priority:");
			scanf("%d", &ptr -> priority);

		}

	}
}


/* Detailed description of the function.
 * This function initialize the linked list
 * @head is input which is the head node of linked list
 * @max_size_of_queue which contains the value of max size of stack or queue
 * @flag contain the value weather list is stack or queue
 * @return : Return the root node which having the new node
 */
static void InsertNodeat(nod **head, int data,  int node_position)
{
	nod *ptr;                          //Temporary pointer having type of nod
	int count = 0;                     //Value of count is zero
	nod *temp;                         //Temporary pointer having type of nod
	nod *new_node;                     //Pointer of new node having type nod
	int PID = 0;                       //Process id
	printf("\nEnter the priority");
	scanf("%d", &PID);
	//Add the new node at first position
	if(node_position == 0)
	{
		ptr = *head;
		temp = *head;
		new_node = (nod*)malloc(sizeof(nod));
		*head = new_node;
		new_node -> number = data;
		new_node -> priority = PID;
		new_node -> next = temp;


	}
	else
	{
		ptr = *head;
		while(ptr != NULL)
		{
			count++;
			if(node_position != count)
			{
				ptr = ptr -> next;
			}
			else
			{
				temp = ptr -> next;
				new_node = (nod*)malloc(sizeof(nod));
				ptr -> next = new_node;
				new_node -> number = data;
				new_node -> priority = PID;
				new_node -> next = temp;
				break;
			}

		}
	}

}

/* Detailed description of the function.
 * This function delete the last node in list
 * @head is input which is the head node of linked list
 * @return : No return value
 */

static void DeleteNode (nod *head)
{
	nod *ptr;                        //Temporary pointer having type of nod
	nod *last_element;               //This pointer points to last element of node
	ptr = head;
	if(head == NULL)
	{
		printf("\nError the list is empty");
		return;
	}
	while(ptr != NULL)
	{
		last_element = ptr;
		ptr = ptr -> next;
	}

	free(last_element);
	last_element = NULL;

}


/* Detailed description of the function.
 * This function delete the node from particular position
 * @head is input which is the head node of linked list
 * @node_position is input from user where user want to delete the node
 * @return : No return value
 */
static void DeleteNodeFrom (nod **head, int node_position)
{
	nod *ptr;            //Temporary pointer having type of nod
	int count = 0;       //Count number of particular nod
	nod *temp;           //Temporary pointer having type of nod

	if(head == NULL)
	{
		printf("\nError The list is empty");
		return;
	}
	/*Delete the node at first position */
	if(node_position == 0)
	{
		ptr = *head;
		*head = ptr -> next;
		free(ptr);
		ptr = NULL;

	}
	else
	{
		ptr = *head;
		/*Delete the node at intermediate position */
		while(ptr != NULL)
		{
			if(node_position != count)
			{
				temp = ptr;                 //temp is previous value and ptr is next value
				ptr = ptr -> next;          //make ptr points to next node
			}
			else
			{
				temp -> next = ptr -> next;  //When we reached to a perticular node at that time the next address of previous pointer(temp) is next address of next pointer					   (ptr) so just store the next address of next node to previos node
				free(ptr);                   //Free the perticular memory of node
				ptr = temp -> next;          //store the next address
				break;
			}
			count++;
		}
	}

}

/* Detailed description of the function.
 * This function prints the list
 * @head is input which is the head node of linked list
 * @return : No return value
 */
static void print_list(nod **head)
{
	int count = 0;         //Count number of particular nod
	nod *ptr;              //Temporary pointer having type of nod
	ptr = *head;
	if(*head == NULL)
	{
		printf("\nThe list is empty");
		return;
	}
	while(ptr != NULL)
	{
		printf("\nlink[%d]:%d", count, ptr->number);
		count++;
		ptr = ptr->next;
	}

}

/* Detailed description of the function.
 * This function deletes the whole link list
 * @head is input which is the head node of linked list
 * @return : No return value
 */
static void deletelist (nod **head)
{
	nod *ptr;
	ptr = *head;
	if(*head == NULL)
	{
		printf("\nThe list is empty");
		return;
	}
	while(ptr != NULL)
	{
		ptr = ptr -> next;
		free(ptr);
	}
	*head = NULL;
}

/* Detailed description of the function.
 * This function Itrative method to generate reverce list
 * @head is input which is the head node of linked list
 * @return : No return value
 */
static void ReverseList(nod **head)
{
	nod *ptr;       //Temporary pointer having type of nod
	nod *tail;      //Temporary pointer having type of nod points to previous node
	nod *temp;      //Temporary pointer points to next node
	ptr = *head;
	tail = NULL;

	while(ptr != NULL)
	{
		temp = ptr -> next;
		ptr -> next = tail;
		tail = ptr;
		ptr = temp;
	}
	*head = tail;
}
nod *head1;


/* Detailed description of the function.
 * This function recurs ice method to generate reverse list
 * @prev is previous node of previous link
 * @curr is current node of present link
 * @return : No return value
 */
void ReverseList_Recursive(nod *prev, nod *curr)
{
	if(curr != NULL)
	{
		ReverseList_Recursive(curr, curr -> next);
		curr -> next = prev;
		return;
	}
	else
	{
		head1 = prev;
	}

}


/* Detailed description of the function.
 * This function gives the value of how many number
 * of links are there in linked list
 * @head is input which is the head node of linked list
 * @return : Return int value of size
 */
/*Recursive method to generate list */
static int getListlSize (nod *head)
{
	if(head == NULL)
	{
		return 0;
	}
	return 1 + getListlSize(head -> next);
}



/* Detailed description of the function.
 * This function swap two elements in the list
 * @previous_element is previous node of list
 * @next_element is current node of list
 * @return : No Return value
 */
/*Swap function is used to swap two element in list*/
void swap(nod *previous_element, nod *next_element)
{
	int temp;                               //Which stores the value of previous node data
	int temp_priority;                       //Which stores the value of previous node  priority
	temp = next_element -> number;
	temp_priority = next_element -> priority;
	next_element -> number = previous_element -> number;
	next_element -> priority = previous_element -> priority;
	previous_element -> number = temp;
	previous_element -> priority = temp_priority;
}

/* This function gives the execution time in ms*/
float timedifference_msec(struct timeval t0, struct timeval t1)
{
	return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}

/*Logic:
  ->Take first element of list then compare that element with rest of list so at right most side we got maximum value
  ->So next time take first value from list and compare this value with rest value if it is > next value then swap it
  ->So from this sort we set the previous element of right most side
  ->So itrate this loop till the number of element
 */

/* Detailed description of the function.
 * This function sort elements in the linked list using bubble sort and
 * according to priority vise
 * @head is input which is the head node of linked list
 * @size is an input which show the value
 * @return : No Return value
 */
void bubble_sort(nod *head, int size)
{
	int i = 0;                //i is used for itretion
	int round = 0;            //Number of rounds
	nod *ptr;                 //ptr is pointer used to travers the data
	ptr = head;               //ptr is poited to head
	/*Logic:*/
	for(round = 0 ; round < size ; round++)
	{
		for(i = 0 ; i < (size - 1 ) - round ; i++)
		{
			if((ptr-> priority) > ((ptr -> next) -> priority))
			{
				swap(ptr, ptr -> next);
			}
			if((ptr-> priority) == ((ptr -> next) -> priority))
			{
				if((ptr-> number) > ((ptr -> next) -> number))
				{
					swap(ptr, ptr -> next);
				}
			}
			ptr = ptr -> next;
		}
		ptr = head;
	}

}


/* Detailed description of the function.
 * This function sort elements of linked list according to priority vise.
 * @size is an input which show the value
 * @return : No Return value
 */
/*This function will sort the data accordingly lsb,...,msb*/
void radix_sort_for_priority_queue(nod *head, int size)
{
	int i = 0;                	//i is used for itretion
	int round = 0;           	//Number of rounds
	nod *ptr;                 	//ptr is pointer used to travers the data
	ptr = head;               	//ptr is poited to head
	int prev_number = 0;            //store lsb of previous number
	int next_number = 0;            //store lsb of next number
	int max_data = 0;               //Store the maximum number in linked list
	int digit_count = 0;            //Count the number of digit of max_data
	int n = 10;                     //First devider for radix sort
	while(ptr -> next != NULL)
	{
		if((ptr -> number) >= max_data)
		{
			max_data = ptr -> number;
		}
		ptr = ptr -> next;

	}
	ptr = head;
	//printf("\nHighest number is %d",max_data);
	while(max_data != 0)
	{
		max_data /= 10;
		digit_count++;
	}
	//printf("\nHighest number of digits in linked list %d",digit_count);
	/*Logic:
	  ->Take first element of list then compare that element with rest of list so at right most side we got maximum value
	  ->So next time take first value from list and compare this value with rest value if it is > next value then swap it
	  ->So from this sort we set the previous element of right most side
	  ->So itrate this loop till the number of element
	  ->Then itrate this loop till lsb...msb
	 */
	while(digit_count != 0)
	{
		for(round = 0 ; round < size ; round++)
		{
			for(i = 0 ; i < (size - 1 ) - round ; i++)
			{
				prev_number = (ptr -> priority) % n;
				next_number = ((ptr -> next) -> priority) % n;
				if(prev_number > next_number)
				{
					swap(ptr, ptr -> next);
				}
				ptr = ptr -> next;
			}
			ptr = head;
		}
		digit_count --;
		n *= 10;
	}
	print_list(&head);


}



/* Detailed description of the function.
 * This function sort elements in the linked list using radix sort method
 * @head is input which is the head node of linked list
 * @size is an input which show the value
 * @return : No Return value
 */
/*This function will sort the data accordingly lsb,...,msb*/
void radix_sort(nod *head, int size)
{
	int i = 0;                	//i is used for itretion
	int round = 0;           	//Number of rounds
	nod *ptr;                 	//ptr is pointer used to travers the data
	ptr = head;               	//ptr is poited to head
	int prev_number = 0;            //store lsb of previous number
	int next_number = 0;            //store lsb of next number
	int max_data = 0;               //Store the maximum number in linked list
	int digit_count = 0;            //Count the number of digit of max_data
	int n = 10;                     //First devider for radix sort
	while(ptr != NULL)
	{
		if((ptr -> number) >= max_data)
		{
			max_data = ptr -> number;
		}
		ptr = ptr -> next;

	}
	ptr = head;
	//printf("\nHighest number is %d",max_data);
	while(max_data != 0)
	{
		max_data /= 10;
		digit_count++;
	}
	//printf("\nHighest number of digits in linked list %d",digit_count);
	/*Logic:
	  ->Take first element of list then compare that element with rest of list so at right most side we got maximum value
	  ->So next time take first value from list and compare this value with rest value if it is > next value then swap it
	  ->So from this sort we set the previous element of right most side
	  ->So itrate this loop till the number of element
	  ->Then itrate this loop till lsb...msb
	 */
	while(digit_count != 0)
	{
		for(round = 0 ; round < size ; round++)
		{
			for(i = 0 ; i < (size - 1 ) - round ; i++)
			{
				prev_number = (ptr -> number) % n;
				next_number = ((ptr -> next) -> number) % n;
				if(prev_number > next_number)
				{
					swap(ptr, ptr -> next);
				}
				ptr = ptr -> next;
			}
			ptr = head;
		}
		digit_count --;
		n *= 10;
	}


}


/* Detailed description of the function.
 * This function itrates the node till given mid element
 * @head is input which is the head node of linked list
 * @mid is an input which show the mid element value
 * @return : Return pointer of structure which has value of mid element
 */
/*Here we are itrating our list till middle element of linked list */
nod *itrateNode(nod *head, int mid)
{
	nod *ptr;                        //Temporary pointer having type of nod
	ptr = head;
	while(mid != 0)
	{
		ptr = ptr -> next;
		mid --;
	}
	return ptr;
}



/* Detailed description of the function.
 * This function search the data in linearly in the list
 * @head is input which is the head node of linked list
 * @data is an input which element should be found in list
 * @return : Return flag weather the value is present or not in list
 */
/*In linear search we are searching particular node from 0 to n nodes*/
int LinearSearch(nod *head, int data)
{
	nod *ptr;                   //Temporary pointer having type of nod
	int flag = 0;               //Flag show weather the value is present or not
	ptr = head;

	while(ptr != NULL)
	{
		if(ptr -> number == data)
		{
			flag++;
		}
		ptr = ptr -> next;
	}
	return flag;
}



/* Detailed description of the function.
 * This function search the data using binary search in the list
 * @head is input which is the head node of linked list
 * @data is an input which element should be found in list
 * @return : Return flag weather the value is present or not in list
 */
/*In linear search we are searching particular node from 0 to n nodes*/
/*This method is used to find the sorted data from the list*/
int BinarySearch(nod *head, int data)
{
	nod *ptr;                            //Temporary pointer having type of nod
	int flag = 0;                        //Flag show weather the value is present or not
	int sizeoflist = getListlSize(head); //sizeoflist contain how many nodes are presernt in node
	int mid = 0;                        //Contain the value of middle  element
	int high = sizeoflist;              //Initialize the high as last node
	int low = 0;                        //Initialize the 0 in low value
	do
	{
		mid = (low + high) / 2;
		ptr = itrateNode(head, mid);

		if(ptr -> number == data)
		{
			flag = 1;
			return flag;
		}
		else if( ptr -> number > data)
		{
			high = mid - 1;
		}
		else
		{
			low = mid + 1;
		}

	}while(high >= low);
	return flag;
}



/* Detailed description of the function.
 * This function is driver function of different type of searching
 * @head is input which is the head node of linked list
 * @data is an input which element should be found in list
 * @searching_method is an input gives options for different type of searching
 * @return : Return flag weather the value is present or not in list
 */
/*This function is used to search the data into linked list*/
void SearchData ( nod *head, int data, int searching_method)
{
	//Flag show weather the value is present or not
	int flag;
	if(head == NULL)
	{
		printf("\nThe list is empty");
		return;
	}
	switch(searching_method)
	{
		case 0:
			printf("\nYou choose the linear searching method");
			flag = LinearSearch(head, data);
			if(flag == 0)
			{
				printf("\n %d data is not found in the list",data);
			}
			else
			{
				printf("\n %d data is found in list",data);
			}
			break;

		case 1:
			printf("\nYou choose the binary searching method");
			flag = BinarySearch(head, data);
			if(flag == 0)
			{
				printf("\n %d data is not found in the list",data);
			}
			else
			{
				printf("\n %d data is found in list",data);
			}
			break;

		default:
			printf("\nEnter the valid choice");
			break;
	}
}

int partiton(nod *head, int low, int high)
{
/*******************************************************************************
  *This is a function to implement partition as per each time it's called.
  *It employs partition in the left side for all the elements which are less than pivot.
  *Right hand partition is implemented for all the elements that are greater than pivot.
  *At last the pivot element which is the last element is centered/put at it's correct position.
  ******************************************************************************/
    nod *pivot, *ptr, *temp, *temp1, *temp2;
    ptr = head;
    temp = head;
    temp1 = head;
    int i = 0;
    int j = 0;
    int hi = high;
    printf("\n \\\\high:%d",hi);
   
    pivot = itrateNode(ptr, high);
    //printf("\n\\\\The pivot element is: %d",pivot -> number);

    
    i = (low - 1);
    for(j = low ; j <= high - 1 ; j++)
    {
        temp1 =  itrateNode(temp, j);
        //printf("\nThe j iterate number is %d", temp1 -> number);
        if(temp1 -> number <= pivot -> number)
        {
            i++;
            temp2 = itrateNode(temp, i);
	    //printf("\nThe i iterate number is %d", temp2 -> number);
            swap(temp2, temp1);
	   // printf("\nAfter swaping value of temp1 and temp2");
	   // printf("\ntemp1 %d", temp1 -> number); 
	   // printf("\ntemp2 %d", temp1 -> number); 
        }

    }
    temp2 = itrateNode(temp, i + 1);
    swap(pivot, temp2);
    //printf("The value after the pivot element is: %d",pivot -> number);
    return (i + 1);

}


void quick_sort(nod *qbase, int low, int high){
    /*******************************************************************************
     *This is a function to implement Quick sort algorithm.
     *Here partiton is done as per last element is taken as the pivot element in each partition.
     *There's another function partiton which does the actualm partition algorithm while this function acall itself recursively.
     *This function operates till the left side and right side segment are traversed.
     ******************************************************************************/
    int pi;
    if(low < high)
    {
        pi = partiton(qbase, low, high);
        quick_sort(qbase, low, pi - 1);
        quick_sort(qbase, pi + 1, high);

    }
}

/* Detailed description of the function.
 * This function is a driver function to sort the list
 * @head is input which is the head node of linked list
 * @list_size is input that show the total number of node in list
 * @return : No return value
 */
static void sorting(nod *head, int shorting_method, int list_size)
{
	char name[100];
	if(head == NULL)
	{
		printf("\nError list is empty");
		return;
	}
	printf("\nThe sorting method is:%d",shorting_method);
	switch(shorting_method)
	{
		case 0:
			strcpy(name, "Bubble sort");
			printf("\nYou choose the Bubble short method");
			gettimeofday(&t0, 0);
			bubble_sort(head, list_size);
			gettimeofday(&t1, 0);
			printf("\nAfter shorting we get:");
			print_list(&head);
			break;
		case 1:
			strcpy(name, "Radix sort");
			printf("\nYou choose the Radix short method");
			gettimeofday(&t0, 0);
			radix_sort(head, list_size);
			gettimeofday(&t1, 0);
			printf("\nAfter shorting we get:");
			print_list(&head);
			break;
		case 2:
			strcpy(name, "Quick sort");
			printf("\nYou choose the Quick short method");
			gettimeofday(&t0, 0);
			quick_sort(head, 0, list_size - 1);
			printf("\nAfter shorting we get:");
			gettimeofday(&t1, 0);
			print_list(&head);
			break;
		case 3:
			gettimeofday(&t0, 0);
			radix_sort_for_priority_queue(head, list_size);
			gettimeofday(&t1, 0);
			break;

		default:
			printf("\n Invalid choice");
			break;
	}
	elapsed = timedifference_msec(t0, t1);
	//printf("\n %s executed in %f milliseconds.\n", name, elapsed);
}
/*Priority queue which insert the node at last position in queue and at first position in stack*/
/* Detailed description of the function.
 * This function insert the node according to priority
 * @head is input which is the head node of linked list
 * @data is input that user want to add data into linked list
 * @priority is input that user want to add priority for new node
 * @flag contain the value weather list is stack or queue
 * @return : No return value
 */

static void InsertNode(nod *head, int flag, int priority, int data)
{
	nod *ptr;                         //Temporary pointer having type of nod
	nod *temp;                        //Temporary pointer having type of nod
	ptr = head;
	int check_flag = 0;               //Contains weather list is stack or queue
	switch(flag)
	{
		case 0://QUEUE
			while(ptr != NULL)
			{	if(check_flag == 0)
				{
					if((ptr -> priority) > priority)
					{
						temp = (nod*)malloc(sizeof(nod));
						temp -> next = ptr -> next;
						ptr -> next = temp;
						temp -> number = data;
						temp -> priority = priority;
						ptr = ptr -> next;
						check_flag = 1;
					}
					else
					{
						ptr = ptr -> next;
					}
				}
				else
				{
					ptr = ptr -> next;
				}
			}
			break;
		case 1://STACK
			while(ptr != NULL)
			{
				if(check_flag == 0)
				{
					if((ptr -> priority) > priority)
					{
						temp = (nod*)malloc(sizeof(nod));
						temp -> next = ptr -> next;
						ptr -> next = temp;
						temp -> number = data;
						temp -> priority = priority;
						ptr = ptr -> next;
					}
					else
					{
						ptr = ptr -> next;
					}
				}
				else
				{
					ptr = ptr -> next;
				}
			}
			break;
		default:
			printf("\nEnter the valid choice");
			break;
	}


}
int main()
{

	int number_of_link = 0;          //Contain total number of links
	int que_stack = 0;               //Contain weather list is queue or stack
	int position = 0;                //Contains the value of position
	int new_data = 0;                //Contains the value of new data
	int number_of_node = 0;          //Contain total number of links
	int choice = 0;                  //Contain the value of choice
	int size_of_list = 0;            //Contains the size of linked list
	int user_choice = 0;             //Contain the value of user choice
	int data_found = 0;              //Contain the value of data to be found
	int flag = 0;                    //Contain weather found node is present into list or not
	int priority = 0;                //Contain the value of priority
	head1 = NULL;
	while(1)
	{

		printf("\n\nPriority Singly linked list");
		printf("\n0:Init the linked list");
		printf("\n1:Add the new node in linked list");
		printf("\n2:Delete the node at first position in linked list");
		printf("\n3:Delete the node in linked list");
		printf("\n4:The size of linked list");
		printf("\n5:Reverce the linked list");
		printf("\n6:Delete the linked list");
		printf("\n7:Print the linked list");
		printf("\n8:Sort the linked list");
		printf("\n9:Finding the data elements in the linked list");
		printf("\n10:Insert a node at based on priority");
		printf("\n\nEnter your choice:");
		scanf("%d",&user_choice);
		switch(user_choice)
		{
			case 0:
				printf("\nEnter the number of link:");
				scanf("%d",&number_of_link);
				while(1)
				{
					printf("\nEnter weather you want to make any queue or stack");
					printf("\n1:stack");
					printf("\n0:queue");
					printf("\nEnter your choice:");
					scanf("%d", &que_stack);
					if((que_stack != 0) && (que_stack != 1))
					{
						printf("\nError plese give valid choice:");
					}
					else
					{
						break;
					}
				}
				Initlinklist(&head1, number_of_link, que_stack);
				print_list(&head1);
				break;
			case 1:
				printf("\nEnter at which position you want to add the new data:");
				scanf("%d", &position);
				if(position < number_of_link || position == 0)
				{
					printf("Enter the data:");
					scanf("%d",&new_data);
					InsertNodeat(&head1, new_data, position);
					printf("\nNew node add sucessfully");
					print_list(&head1);
				}
				else
				{
					printf("\nError Cant add at %d position",position);
				}
				break;

			case 2:
				printf("\nDelete the node");
				DeleteNode(head1);
				if(head1 != NULL)
				{
					printf("\nLast node is sucessfully deleted");
					print_list(&head1);
				}
				break;

			case 3:
				printf("\nEnter at which position you want to delete the data");
				scanf("%d", &position);
				if(position < (number_of_link))
				{
					printf("\nHello worls");
					DeleteNodeFrom(&head1, position);
					printf("\nNew node delete sucessfully");
					print_list(&head1);
				}
				else
				{
					printf("\nError Cant delete at %d position",position);
				}
				break;

			case 4:
				size_of_list = getListlSize(head1);
				printf("\nThe size of list is %d:",size_of_list);
				print_list(&head1);
				break;

			case 5:
				printf("\nBy using recursive function");
				ReverseList_Recursive(NULL, head1);
				printf("\nBy using itretive method");
				ReverseList(&head1);
				print_list(&head1);
				break;

			case 6:
				deletelist(&head1);
				printf("\nThe linked list is sucessfully deleted");
				print_list(&head1);
				break;

			case 7:
				print_list(&head1);
				break;

			case 8:
				number_of_node = getListlSize(head1);
				printf("\nNumber of nodes are:%d",number_of_node);
				printf("\nSorting methods");
				printf("\n0 for Bubble sort for priority queue");
				printf("\n1 for Radix sort");
				printf("\n2 for Quick sort");
				printf("\n3 for Radix sort for priority queue");
				printf("\nEntre your choice:");
				scanf("%d", &choice);
				sorting(head1, choice, number_of_node);

				break;
			case 9:
				number_of_node = getListlSize(head1);
				printf("\nEnter the data to be found");
				scanf("%d", &data_found);
				printf("\n0:Linear Search");
				printf("\n1:Binary Search");
				printf("\nEnter the sorting method");
				scanf("%d", &choice);
				if(choice == 1)
				{
					sorting(head1, choice, number_of_node);
					SearchData (head1, data_found, choice);
				}
				else
				{
					SearchData (head1, data_found, choice);
				}
				break;
			case 10:
				flag = que_stack;
				number_of_node = getListlSize(head1);
				printf("\nEnter the number to be inserted:");
				scanf("%d", &new_data);
				printf("\nEnter the priority of new number:");
				scanf("%d", &priority);
				sorting(head1, 3, number_of_node);
				InsertNode(head1, flag, priority, new_data);
				break;

			default:
				printf("\nEnter the valid choice");
				break;
		}
	}


	return 0;
}


