/*******************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:Sorting
 * @file
 *
 *
 *l;
 * Author       - Dhruv N Shah
 *
 *******************************************************************************
 *
 * History
 *
 * Apr/15/2018, Dhruv N
 *
 * Apr/15/2018, Dhruv N, Created (description)
 *
 ******************************************************************************/
/*******************
 * Includes
 *******************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>


struct Node{
	int number;
	struct Node *next;
};
typedef struct Node nod;

/***********************
 * Global Variables
 ***********************/
struct timeval t0;
struct timeval t1;
float elapsed;
nod *head1;

/************************
 * Function Prototypes
 ************************/
/* static void Initlinklist(nod **head, int max_size_of_queue, int flag)
   that initialise the linked list in form of stack or queue*/
 /* Detailed description of the function.
 *
 * @head is double pointer which is used to point the first node of list
 * @max_size_of_queue is input which gives the total number of node is inserted into the list
 * @flag is input to choose weather list is queue or stack
 * @No return
 */
static void Initlinklist(void **head, int max_size_of_queue, int flag)
{

	nod *ptr;              //take one temporary pointer to structure variable
	int loop = 0;          //loop is for itretion
	if(flag == 0)                                             //QUEUE
	{
		for(loop = 0 ; loop < max_size_of_queue ; loop++)
		{
		    /*If head is null then list is empty then add first node */
			if(loop == 0)
			{
				*head = (nod *)malloc(sizeof(nod));
				ptr = (nod *)(*head);
			}
			/*Add the new node at last */
			else
			{
				ptr -> next = (nod *)malloc(sizeof(nod));
				ptr = ptr -> next;
			}
			printf("\nEnter the data:");
			scanf("%d", &ptr->number);
		}
		ptr->next = NULL;
	}
	else                     				//STACK
	{

		for(loop = 0 ; loop < max_size_of_queue ; loop++)
		{
		    /*First allocate the memory to new node
            1>Here address of previous node is store in to head so give
            the next address of new node as address of head.
            2>now point the head node to a new node
            */
			ptr = (nod *)malloc(sizeof(nod));
			ptr -> next = (nod *)(*head);
			*head = ptr;
			printf("\nEnter the data:");
			scanf("%d", &ptr->number);
			printf(" %d -> ",((nod *)(*head)) -> number);
		}

	}
}
/* Detailed description of the function.
 * This function initialize the linked list
 * @head is input which is the head node of linked list
 * @max_size_of_queue which contains the value of max size of stack or queue
 * @flag contain the value weather list is stack or queue
 * @return : Return the root node which having the new node
 */
static void InsertNodeat(void **head, int data,  int node_position)
{
	nod *ptr;
	int count = 0;
	nod *temp;
	nod *new_node;
	/*If the user add the node at first position*/
	/*Steps 1> store the address of head in temp variable
	2>allocate the memory for new node
	3>now make a link between new node and temp
	4>Point the head at new node
	*/
	if(node_position == 0)
	{
		temp = (nod *)(*head);
		new_node = (nod*)malloc(sizeof(nod));
		*head = new_node;
		new_node -> number = data;
		new_node -> next = temp;
		return;

	}
	ptr = (nod *)(*head);
	while(ptr != NULL)
	{
		count++;
		if(node_position != count)
		{
			ptr = ptr -> next;
		}

	/*If the user add the node at intermediate position*/
	/*Steps 1> store the address of particular node in temp variable
	2>allocate the memory for new node
	3>break the link between two node and add the new node between them
	4>So give the next address of ptr as new node
	5>Give next address of new node as temp
	*/
		else
		{
			temp = ptr -> next;
			new_node = (nod*)malloc(sizeof(nod));
			ptr -> next = new_node;
			new_node -> number = data;
			new_node -> next = temp;
		}

	}

}

/* Detailed description of the function.
 * This function delete the last node in list
 * @head is input which is the head node of linked list
 * @return : No return value
 */
static void DeleteNode (void *head)                   //single time traversing last node should be deleted
{
	nod *ptr;                       //Temporary pointer having type of nod
	nod *temp;                      //This pointer points to last element of node
	ptr = (nod *)(head);
	/*When list is empty at that time print error message*/
	if(head == NULL)
	{
		printf("\nError the list is empty");
		return;
	}
	/*Itrate loop till second last element come*/
	while(ptr -> next != NULL)
	{
		temp = ptr;
		ptr = ptr -> next;
	}
	/*Free allocated memory to last node */

	free(temp -> next);
	temp -> next = NULL;

}

/*According to a position it will delete the node */
/* Detailed description of the function.
 * This function delete the node from particular position
 * @head is input which is the head node of linked list
 * @node_position is input from user where user want to delete the node
 * @return : No return value
 */
static void DeleteNodeFrom (void **head, int node_position)
{
	nod *ptr;                        //Temporary pointer having type of nod
	int count = 0;                   //Count number of particular nod
	nod *temp;                        //Temporary pointer having type of nod

    /*If list is empty then print error message */
	if(head == NULL)
	{
		printf("\nError The list is empty");
		return;
	}

	/*If the user choose the first position so designing steeps are
	1>store the address of head to pointer variable.
	2>Next give the next address of ptr = head
	3>then free the allocated memory
	*/
	if(node_position == 0)
	{
		ptr = (nod *)(*head);
		*head = ptr -> next;
		free(ptr);
		ptr = NULL;
		//ptr = (nod *)(*head);
		//count++;
		return;

	}
	/*If user delete the data at intermediate location of queue.*/

	ptr = (nod *)(*head);                 //ptr is pointing to head
	while(ptr != NULL)
	{
        count++;
		if(node_position != count)
		{
			temp = ptr;                 //temp is previous value and ptr is next value
			ptr = ptr -> next;          //make ptr points to next node
		}
		else
		{
			temp -> next = ptr -> next;  //When we reached to a perticular node at that time the next address of previous pointer(temp) is next address of next pointer					   (ptr) so just store the next address of next node to previos node
			free(ptr);                   //Free the perticular memory of node
			ptr = NULL;
			ptr = temp -> next;          //store the next address

		}

	}

}
/* Detailed description of the function.
 * This function prints the list
 * @head is input which is the head node of linked list
 * @return : No return value
 */
/*Print the linked list*/
static void print_list(void **head)
{
	int count = 0;                          //Count number of particular nod
	nod *ptr;                               //Temporary pointer having type of nod
	ptr = ((nod *)(*head));
	/*If pointer is NULL then print the error message */
	if(ptr == NULL)
	{
		printf("\nThe list is empty");
		return;
	}
	/*Itrarte the loop until NULL will not come */
	while(ptr != NULL)
	{
		printf("\nlink[%d]:%d", count, ptr->number);
		count++;
		ptr = ptr->next;
	}

}

/*Delete the linked list*/
/* Detailed description of the function.
 * This function deletes the whole link list
 * @head is input which is the head node of linked list
 * @return : No return value
 */
static void deletelist (void **head)
{
	nod *ptr;                               //Temporary pointer having type of nod
	ptr = (nod *)(*head);                   //Store address of head to ptr
	nod *temp;                              //Temporary pointer having type of nod
	/*If head is NULL then list */
	if(*head == NULL)
	{
		printf("\nThe list is empty");
		return;
	}
	while(ptr != NULL)
	{
		temp = ptr -> next;             //First the next address of link is store in temp
		free(ptr); 			            //Then free the allocated memory while creating the linked list
		ptr = temp;			            //Give the temp address to current pointer
	}
	*head = NULL;
}
/* Detailed description of the function.
 * This function Itrative method to generate reverse list
 * @head is input which is the head node of linked list
 * @return : No return value
 */

static void ReverseList(void **head)
{
	nod *ptr;                               //Temporary pointer having type of nod
	nod *tail;                              //Temporary pointer having type of nod points to previous node
	nod *temp;                              //Temporary pointer points to next node
	ptr = (nod *)(*head);
	tail = NULL;
    /*Steps to design reverse list
    1>store the next address of ptr to temp variable
    2>make a new link between ptr and tail.
    3>Now tail is pointing to ptr
    4>And ptr is pointing to temp
    */
	while(ptr != NULL)
	{
		temp = ptr -> next;
		ptr -> next = tail;
		tail = ptr;
		ptr = temp;
	}
	*head = tail;
}

/*Reverse the list using recursive method*/
/* Detailed description of the function.
 * This function recurs ice method to generate reverse list
 * @prev is previous node of previous link
 * @curr is current node of present link
 * @return : No return value
 */
 /*Logic part:
 First there is current element and previous element.
 1>Iterate the loop until the current element became NULL
 Then go to recursive function and reach to last node.
 2>Then point the head at last element
 3>Now recursive function return at that time we make link between current element
 and previous element
 */
void ReverseList_Recursive(nod *prev, nod *curr)
{
	if(curr != NULL)
	{
		ReverseList_Recursive(curr, curr -> next);
		curr -> next = prev;
		return;
	}
	else
	{
		head1 = prev;
	}

}

/* Detailed description of the function.
 * This function gives the value of how many number
 * of links are there in linked list
 * @head is input which is the head node of linked list
 * @return : Return int value of size
 */
/*Recursive method to generate list */
static int getListlSize (nod *head)
{
	if(head == NULL)
	{
		return 0;
	}
	return 1 + getListlSize(head -> next);
}

/* Detailed description of the function.
 * This function swap two elements in the list
 * @previous_element is previous node of list
 * @next_element is current node of list
 * @return : No Return value
 */
void swap(nod *previous_element, nod *next_element)
{
	int temp;                               //Which stores the value of previous node value
	temp = next_element -> number;
	next_element -> number = previous_element -> number;
	previous_element -> number = temp;
}

/* This function gives the execution time in ms*/
float timedifference_msec(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0f + (t1.tv_usec - t0.tv_usec) / 1000.0f;
}


/* Detailed description of the function.
 * This function sort elements in the linked list using bubble sort and
 * according to priority vise
 * @head is input which is the head node of linked list
 * @size is an input which saw the value
 * @return : No Return value
 */
/*Logic:
	->Take first element of list then compare that element with rest of list so at right most side we got maximum value
	->So next time take first value from list and compare this value with rest value if it is > next value then swap it
	->So from this sort we set the previous element of right most side
	->So itrate this loop till the number of element
	*/
void bubble_sort(nod *head, int size)
{
	int i = 0;                //i is used for itretion
	int round = 0;            //Number of rounds
	nod *ptr;                 //ptr is pointer used to travers the data
	ptr = head;               //ptr is poited to head
	/*Logic:*/
	for(round = 0 ; round < size ; round++)
	{
		for(i = 0 ; i < (size - 1 ) - round ; i++)
		{
			if((ptr-> number) > ((ptr -> next) -> number))
			{
				swap(ptr, ptr -> next);
			}
			ptr = ptr -> next;
		}
		ptr = head;
	}

}
/* Detailed description of the function.
 * This function sort elements of linked list according to priority vise.
 * @size is an input which saw the value
 * @return : No Return value
 */
/*This function will sort the data accordingly lsb,...,msb*/
void radix_sort(nod *head, int size)
{
	int i = 0;                	//i is used for itretion
	int round = 0;           	//Number of rounds
	nod *ptr;                 	//ptr is pointer used to travers the data
	ptr = head;               	//ptr is poited to head
	int prev_number = 0;            //store lsb of previous number
	int next_number = 0;            //store lsb of next number
	int max_data = 0;               //Store the maximum number in linked list
	int digit_count = 0;            //Count the number of digit of max_data
	int n = 10;                     //First devider for radix sort
	while(ptr != NULL)
	{
		if((ptr -> number) >= max_data)
		{
			max_data = ptr -> number;
		}
		ptr = ptr -> next;

	}
	ptr = head;
	printf("\nHighest number is %d",max_data);
	while(max_data != 0)
	{
		max_data /= 10;
		digit_count++;
	}
	printf("\nHighest number of digits in linked list %d",digit_count);
	/*Logic:
	->Take first element of list then compare that element with rest of list so at right most side we got maximum value
	->So next time take first value from list and compare this value with rest value if it is > next value then swap it
	->So from this sort we set the previous element of right most side
	->So itrate this loop till the number of element
	->Then itrate this loop till lsb...msb
	*/
	while(digit_count != 0)
	{
		for(round = 0 ; round < size ; round++)
		{
			for(i = 0 ; i < (size - 1 ) - round ; i++)
			{
				prev_number = (ptr -> number) % n;
				next_number = ((ptr -> next) -> number) % n;
				if(prev_number > next_number)
				{
					swap(ptr, ptr -> next);
				}
				ptr = ptr -> next;
			}
			ptr = head;
		}
		digit_count --;
		n *= 10;
	}

}
/* Detailed description of the function.
 * This function itrates the node till given mid element
 * @head is input which is the head node of linked list
 * @mid is an input which saw the mid element value
 * @return : Return pointer of structure which has value of mid element
 */
/*This function is used to find the middle link value */
nod *itrateNode(nod *head, int mid)
{
	nod *ptr;
	ptr = head;
	if( mid < 0)
	{
		return ptr;
	}
	while(mid != 0)
	{
		ptr = ptr -> next;
		mid --;
	}
	return ptr;
}

/* Detailed description of the function.
 * This function search the data in linearly in the list
 * @head is input which is the head node of linked list
 * @data is an input which element should be found in list
 * @return : Return flag weather the value is present or not in list
 */
/*This function is used for linear search */
int LinearSearch(nod *head, int data)
{
	nod *ptr;
	int flag = 0;
	ptr = head;

	while(ptr != NULL)
	{
		if(ptr -> number == data)
		{
			flag++;
		}
		ptr = ptr -> next;
	}
	return flag;
}

/* Detailed description of the function.
 * This function search the data using binary search in the list
 * @head is input which is the head node of linked list
 * @data is an input which element should be found in list
 * @return : Return flag weather the value is present or not in list
 */
/*This method is used to find the sorted data from the list*/
int BinarySearch(nod *head, int data)
{
	nod *ptr;
	int flag = 0;
	int sizeoflist = getListlSize(head);
	int mid = 0;
	int high = sizeoflist;
	int low = 0;
	do
	{
		mid = (low + high) / 2;
		ptr = itrateNode(head, mid);

		if(ptr -> number == data)
		{
			flag = 1;
			return flag;
		}
		else if( ptr -> number > data)
		{
			high = mid - 1;
		}
		else
		{
			low = mid + 1;
		}

	}while(high >= low);
	return flag;
}


/* Detailed description of the function.
 * This function is driver function of different type of searching
 * @head is input which is the head node of linked list
 * @data is an input which element should be found in list
 * @searching_method is an input gives options for different type of searching
 * @return : Return flag weather the value is present or not in list
 */
/*This function is used to search the data into linked list*/
void SearchData ( nod *head, int data, int searching_method)
{
	int flag;
	if(head == NULL)
	{
		printf("\nThe list is empty");
		return;
	}
	switch(searching_method)
	{
		case 0:
			printf("\nYou choose the linear searching method");
			flag = LinearSearch(head, data);
			if(flag == 0)
			{
				printf("\n %d data is not found in the list", data);
			}
			else
			{
				printf("\n %d data is found in list the list", data);
			}
			break;

		case 1:
			printf("\nYou choose the binary searching method");
			flag = BinarySearch(head, data);
			if(flag == 0)
			{
				printf("\n %d data is not found in the list", data);
			}
			else
			{
				printf("\n %d data is found in list the list", data);
			}
			break;

		default:
			printf("\nEnter the valid choice");
			break;
	}
}

int partiton(nod *head, int low, int high)
{
/*******************************************************************************
  *This is a function to implement partition as per each time it's called.
  *It employs partition in the left side for all the elements which are less than pivot.
  *Right hand partition is implemented for all the elements that are greater than pivot.
  *At last the pivot element which is the last element is centered/put at it's correct position.
  ******************************************************************************/
    nod *pivot, *ptr, *temp, *temp1, *temp2;
    ptr = head;
    temp = head;
    temp1 = head;
    int i = 0;
    int j = 0;
    int hi = high;
    printf("\n \\\\high:%d",hi);
   
    pivot = itrateNode(ptr, high);
    //printf("\n\\\\The pivot element is: %d",pivot -> number);

    
    i = (low - 1);
    for(j = low ; j <= high - 1 ; j++)
    {
        temp1 =  itrateNode(temp, j);
        //printf("\nThe j iterate number is %d", temp1 -> number);
        if(temp1 -> number <= pivot -> number)
        {
            i++;
            temp2 = itrateNode(temp, i);
	    //printf("\nThe i iterate number is %d", temp2 -> number);
            swap(temp2, temp1);
	   // printf("\nAfter swaping value of temp1 and temp2");
	   // printf("\ntemp1 %d", temp1 -> number); 
	   // printf("\ntemp2 %d", temp1 -> number); 
        }

    }
    temp2 = itrateNode(temp, i + 1);
    swap(pivot, temp2);
    //printf("The value after the pivot element is: %d",pivot -> number);
    return (i + 1);

}


void quick_sort(nod *qbase, int low, int high){
    /*******************************************************************************
     *This is a function to implement Quick sort algorithm.
     *Here partiton is done as per last element is taken as the pivot element in each partition.
     *There's another function partiton which does the actualm partition algorithm while this function acall itself recursively.
     *This function operates till the left side and right side segment are traversed.
     ******************************************************************************/
    int pi;
    if(low < high)
    {
        pi = partiton(qbase, low, high);
        quick_sort(qbase, low, pi - 1);
        quick_sort(qbase, pi + 1, high);

    }
}



/* Detailed description of the function.
 * This function is a driver function to sort the list
 * @head is input which is the head node of linked list
 * @list_size is input that show the total number of node in list
 * @return : No return value
 */
static void sorting(nod *head, int shorting_method, int list_size)
{
	char name[100];
	if(head == NULL)
	{
		printf("\nError list is empty");
		return;
	}
	printf("\nThe sorting method is:%d",shorting_method);
	switch(shorting_method)
	{
		case 0:
			strcpy(name, "Bubble sort");
			printf("\nYou choose the Bubble short method");
			gettimeofday(&t0, 0);
			bubble_sort(head, list_size);
			gettimeofday(&t1, 0);
			printf("\nAfter shorting we get:");
			print_list((void *)&head);
			break;
		case 1:
			strcpy(name, "Radix sort");
			printf("\nYou choose the Radix short method");
			gettimeofday(&t0, 0);
			radix_sort(head, list_size);
			gettimeofday(&t1, 0);
			printf("\nAfter shorting we get:");
			print_list((void *)&head);
			break;
		case 2:
			strcpy(name, "Quick sort");
			printf("\nYou choose the Quick short method");
			gettimeofday(&t0, 0);
			quick_sort(head, 0, list_size - 1);
			printf("\nAfter shorting we get:");
			gettimeofday(&t1, 0);
			print_list((void *)&head);
			break;
		default:
			printf("\n Invalid choice");
			break;
	}
	elapsed = timedifference_msec(t0, t1);
	printf("\n %s executed in %f milliseconds.\n", name, elapsed);
}

int main()
{

	int number_of_link = 0;              //Contain total number of links
	int que_stack = 0;                   //Contain weather list is queue or stack
	int position = 0;                    //Contains the value of position
	int new_data = 0;                    //Contains the value of new data
	int number_of_node = 0;              //Contain total number of links
	int choice = 0;                      //Contain the value of choice
	int size_of_list = 0;                //Contains the size of linked list
	int user_choice = 0;                 //Contain the value of user choice
	int data_found = 0;                  //Contain the value of data to be found
	head1 = NULL;
	while(1)
	{

		printf("\n\nSingly linked list");
		printf("\n0:Init the linked list");
		printf("\n1:Add the new node in linked list");
		printf("\n2:Delete the node at last position in linked list");
		printf("\n3:Delete the node in linked list");
		printf("\n4:The size of linked list");
		printf("\n5:Reverce the linked list");
		printf("\n6:Delete the linked list");
		printf("\n7:Print the linked list");
		printf("\n8:Sort the linked list");
		printf("\n9:Finding the data elements in the linked list");
		printf("\n\nEnter your choice:");
		scanf("%d",&user_choice);
		switch(user_choice)
		{
			case 0:
				printf("\nEnter the number of link:");
				scanf("%d",&number_of_link);
				while(1)
				{
					printf("\nEnter weather you want to make any queue or stack");
					printf("\n1:stack");
					printf("\n0:queue");
					printf("\nEnter your choice:");
					scanf("%d", &que_stack);
					if((que_stack != 0) && (que_stack != 1))
					{
						printf("\nError plese give valid choice:");
					}
					else
					{
						break;
					}
				}
				Initlinklist((void *)&head1, number_of_link, que_stack);
				print_list((void *)&head1);
				break;
			case 1:
				printf("\nEnter at which position you want to add the new data:");
				scanf("%d", &position);
				if(position < number_of_link || position == 0)
				{
					printf("Enter the data:");
					scanf("%d",&new_data);
					InsertNodeat((void *)&head1, new_data, position);
					printf("\nNew node add sucessfully");
					print_list((void *)&head1);
				}
				else
				{
					printf("\nError Cant add at %d position",position);
				}
				break;

			case 2:
				printf("\nDelete the node");
				DeleteNode((void *)head1);
				if(head1 != NULL)
				{
					printf("\nLast node is sucessfully deleted");
					print_list((void *)&head1);
				}
				break;

			case 3:
				printf("\nEnter at which position you want to delete the data");
				scanf("%d", &position);
				if(position < (number_of_link))
				{
					printf("\nHello worls");
					DeleteNodeFrom((void *)&head1, position);
					printf("\nNew node delete sucessfully");
					print_list((void *)&head1);
				}
				else
				{
					printf("\nError Cant delete at %d position",position);
				}
				break;

			case 4:
				size_of_list = getListlSize((void *)head1);
				printf("\nThe size of list is %d:",size_of_list);
				print_list((void *)&head1);
				break;

			case 5:
				printf("\nBy using recursive method");
				ReverseList_Recursive(NULL, head1);
				print_list((void *)&head1);
				printf("\nBy using iterative method");
				ReverseList((void *)&head1);
				print_list((void *)&head1);
				break;

			case 6:
				deletelist((void *)&head1);
				printf("\nThe linked list is sucessfully deleted");
				print_list((void *)&head1);
				break;

			case 7:
				print_list((void *)&head1);
				break;

			case 8:
				number_of_node = getListlSize(head1);
				printf("\nNumber of nodes are:%d",number_of_node);
				printf("\nSorting methods");
				printf("\n0 for Bubble sort");
				printf("\n1 for Radix sort");
				printf("\n2 for Quick sort");
				printf("\nEntre your choice:");
				scanf("%d", &choice);
				sorting(head1, choice, number_of_node);

				break;
			case 9:
				number_of_node = getListlSize(head1);
				printf("\nEnter the data to be found");
				scanf("%d", &data_found);
				printf("\n0:Linear Search");
				printf("\n1:Binary Search");
				printf("\nEnter the sorting method");
				scanf("%d", &choice);
				if(choice == 1)
				{
					sorting(head1, choice, number_of_node);
					SearchData (head1, data_found, choice);
				}
				else
				{
					SearchData (head1, data_found, choice);
				}
				break;

			default:
				printf("\nEnter the valid choice");
				break;
		}
	}


	return 0;
}
