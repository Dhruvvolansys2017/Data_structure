/*******************************************************************************
 * Copyright(c) <2018>, Volansys Technologies
 *
 * Description:Circular doubly linked list
 * 
 *
 * Author       - Dhruv N Shah
 *
 *******************************************************************************
 *
 * History
 *
 * Apr/15/2018, Dhruv N
 *
 * Apr/15/2018, Dhruv N, Created (description)
 *
 ******************************************************************************/
/*******************
 * Includes
 *******************/
#include <stdio.h>
#include<stdlib.h>
/*************************
 * Structure
 *************************/
//Node declaration
struct Node
{
	int data;                   //Contain particular node number
	struct Node *next;          //Having the address of previous node
	struct Node *prev;          //Having the address of previous node
};

/***********************
 * Global Variables
 ***********************/
typedef struct Node nod;        //Make one user define function of structure Node

/* Detailed description of the function.
 * This function insert the node at the end of the linked list
 * @head is input which is the head node of linked list
 * @value is input data which is entered by the user
 * @return : No Return value
 */
void insertEnd(nod ** head, int value)
{
	nod *new_node;                            //Pointer of new node having type nod
	new_node = (nod *)malloc(sizeof(nod));    //Allocate the memory
	/*logic part*/
	/*If head is NULL add the first node
	  1>New node of next and previous address = address of new node
	  2>point the head address to new node address
	 */
	if (*head == NULL)
	{
		new_node -> data = value;
		new_node -> next = new_node -> prev = new_node;
		*head = new_node;
	}
	/*logic part:*/
	/*When we insert the node at last:
	  1>Copy the address of last node in last pointer variable
	  2>To add the new node at last then give the previous address = last pointer variable
	  3>Give the next address of last pointer variable as new node  (last to last)
	  3>New node next address is the address of head pointer
	  4>head previous address become new node address
	 */
	else
	{
		nod *last = (*head) -> prev;
		new_node -> data = value;
		new_node -> next = *head;
		(*head) -> prev = new_node;
		new_node -> prev = last;
		last->next = new_node;
	}
}

/* Detailed description of the function.
 * This function insert the node at the beginning of the linked list
 * @head is input which is the head node of linked list
 * @value is input data which is entered by the user
 * @return : No Return value
 */
void insertBegin(nod **head, int value)
{
	nod *last;                                      //Pointer of last node having type nod
	nod *new_node = (nod *)malloc(sizeof(nod));    //Allocate the memory
	new_node -> data = value;
	/*logic part*/
	/*If head is NULL add the first node
	  1>New node of next and previous address = address of new node
	  2>point the head address to new node address
	 */
	if (*head == NULL)
	{
		new_node -> data = value;
		new_node -> next = new_node -> prev = new_node;
		*head = new_node;
	}
	/*logic part:*/

	/*When we insert the node at beginning:
	  1>Copy the address of last node in last pointer variable
	  2>To add the new node at beginning then give the previous address = last pointer variable
	  3>Give the next address of last pointer variable as new node
	  3>New node next address is the address of head pointer
	  4>head pointer address become new node address
	 */
	else
	{
		last = (*head) -> prev;
		new_node -> next = *head;
		new_node -> prev = last;
		(*head) -> prev = new_node;
		*head = new_node;
	}
}


/* Detailed description of the function.
 * This function delete the node at the beginning of the circular doubly linked list
 * @head is input which is the head node of linked list
 * @return : No Return value
 */
void deleteatbegin(nod **head)
{
	nod *last;                          //Pointer of last node having type nod
	nod *delete_node;
	/*logic part*/
	/*If head is NULL then list is empty
	 */

	if(head == NULL)
	{
		printf("\nThe list is empty");
	}
	/*logic part:*/
	/*To delete the first node
	  1>First copy the address of last node in last pointer variable
	  2>Give the address of head to Delete node
	  3>give the next address of last pointer as next address of delete node
	  so we break the link between first node and last node
	  4>Now head pointer is pointing to next address of delete node
	  5>Make a link between head and last node
	  6>Free the allocated memory in Delete node
	 */
	else
	{
		last = (*head) -> prev;
		delete_node = (*head);
		last -> next = delete_node -> next;
		(*head) = delete_node -> next;
		(*head) -> prev = last;
		free(delete_node);
		delete_node = NULL;
	}
}


/* Detailed description of the function.
 * This function delete the node at the last of the circular doubly linked list
 * @head is input which is the head node of linked list
 * @return : No Return value
 */
void deleteatend(nod **head)
{
	nod *last;                          //Pointer of last node having type nod
	/*logic part*/
	/*If head is NULL then list is empty
	 */
	if(head == NULL)
	{
		printf("\nThe list is empty");
	}
	/*logic part:*/

	/*logic part:*/
	/*To delete the first node
	  1>First copy the address of last node in last pointer variable
	  2>Make a link between head and previous to previous node
	  3>Make link between previous to previous node and head
	  6>Free the allocated memory in last
	 */
	else
	{
		last = (*head) -> prev;
		(*head) -> prev = last -> prev;
		(last -> prev) -> next = (*head);
		free(last);
		last = NULL;
	}
}
/* Detailed description of the function.
 * This function delete the node at particular location of the circular doubly linked list
 * @head is input which is the head node of linked list
 * @position is input at which position we want to delete the node
 * @return : No Return value
 */
void deleteatAfter(nod **head, int position)
{
	nod *last;              //Pointer of last node having type nod
	nod *delete_node;
	nod *temp;              //Pointer of temp node having type nod

	last = (*head) -> prev;
	int count = 0;          //Make count as 0 which counts the total number of node
	temp = (*head);
	while(temp -> next != (*head))   //Itrates the loop till same head address will not come
	{
		temp = temp -> next;
		count++;                    //Count the node in circular doubly list
	}
	/*Position is greater than count then there is error */
	if(position > count)
	{
		printf("\nError please enter valid position");
	}
	/*Position of first node */
	if(position == 0)
	{
		if(head == NULL)
		{
			printf("\nThe list is empty");
		}
		/*logic part:"*/
		else
		{
			last = (*head) -> prev;
			delete_node = (*head);
			last -> next = delete_node -> next;
			(*head) = delete_node -> next;
			(*head) -> prev = last;
			free(delete_node);
			delete_node = NULL;
		}
	}
	/*Position of last node */
	else if(position == (count))
	{

		if(head == NULL)
		{
			printf("\nThe list is empty");
		}
		/*logic part:*/
		else
		{
			last = (*head) -> prev;
			(*head) -> prev = last -> prev;
			(last -> prev) -> next = (*head);
			free(last);
			last = NULL;
		}

	}
	/*Position of intermediate node */
	else
	{
		count = 0;
		temp = *head;
		while(temp -> next != (*head))
		{

			if(position != count)
			{
				//printf("\nInsert node:%d",temp->data);
				temp = temp -> next;
			}
			/*logic part:*/
			/*Position of intermediate node
			  1>Store particular node in delete node
			  2>Make a link between previous of delete node and next node of temp
			  So here we break the link delete node and temp
			  3>Make the link between temp of next and previous of delete node
			  4>Free Delete node
			 */
			else
			{
				delete_node = temp;
				(delete_node -> prev) -> next = temp -> next;
				temp -> next = delete_node -> prev;
				free(delete_node);
				delete_node = NULL;
				temp = temp -> next;
				break;
			}
			count++;
		}

	}

}
/* Detailed description of the function.
 * This function insert the node at particular location of the circular doubly linked list
 * @head is input which is the head node of linked list
 * @position is input at which position we want to insert the node
 * @return : No Return value
 */

void insertAfter(nod **head, int position, int value)
{
	nod *temp;				 //Pointer of temp node having type nod
	nod *new_node;				 //Pointer of new node having type nod
	nod *store1;				 //Pointer of store1 node having type nod
	int count = 0;				 //count the node in circular doubly linked list
	temp = (*head);
	while(temp -> next != (*head))
	{
		temp = temp -> next;
		count++;
	}
	if(position > count)
	{
		printf("\nError please enter valid position");
	}
	/*Position of first node */
	if(position == 0)
	{
		new_node = (nod *)malloc(sizeof(nod));
		new_node -> data = value;
		if (*head == NULL)
		{
			new_node->data = value;
			new_node->next = new_node->prev = new_node;
			*head = new_node;
		}
		/*logic part:*/
		else
		{
			nod *last = (*head)->prev;
			new_node->next = *head;
			new_node->prev = last;
			last->next = (*head)->prev = new_node;
			*head = new_node;
		}
	}
	/*Position of last node */
	else if(position == count)
	{
		new_node = (nod *)malloc(sizeof(nod));
		if (*head == NULL)
		{
			new_node->data = value;
			new_node->next = new_node->prev = new_node;
			*head = new_node;
		}/*logic part:*/
		else
		{
			nod *last = (*head)->prev;
			new_node->data = value;
			new_node->next = *head;
			(*head)->prev = new_node;
			new_node->prev = last;
			last->next = new_node;
		}
	}
	/*Position of intermediate node */

	else
	{
		new_node = (nod *)malloc(sizeof(nod));         //Allocate the new memory in new_node
		new_node -> data = value;
		count = 0;					//Initial count is 0 
		temp = *head;
		while(temp -> next != (*head))
		{
			if(position != count)
			{
				//printf("\nInsert node:%d", temp->data);
				temp = temp -> next;
			}

			/*logic part:*/
			/*Position of intermediate node
			  1>Store particular node in store1 node
			  2>Make a link between previous node of temp and new_node
			  3>Make the link between new_node of previous and temp
			  4>make a link between new node and temp
			  5>make a link between temp and new_node
			 */
			else
			{

				store1 = (temp -> prev) -> next;
				(temp -> prev) -> next = new_node;
				new_node -> next = temp;
				temp -> prev = new_node;
				new_node -> prev = store1;
				temp = temp -> next;
				break;
			}
			count++;
		}

	}

}

/* Detailed description of the function.
 * This function print the circular doubly linked list
 * @head is input which is the head node of linked list
 * @return : No Return value
 */
void print(nod *head)
{
	nod *temp = head;				 //Pointer of temp node having type nod

	printf("\nLink list is \n");
	while (temp->next != head)			//Itrates the loop till same head address will not came
	{
		printf("%d -> ", temp->data);
		temp = temp->next;
	}
	printf("%d ", temp->data);
	printf("\n");

}

/* Detailed description of the function.
 * This function print reverse the circular doubly linked list
 * @head is input which is the head node of linked list
 * @return : No Return value
 */
void Reverse_print(nod *head)
{
	nod *temp;					 //Pointer of temp node having type nod
	printf("\nLink list in reverse order\n");
	nod *last = head->prev;				//last nod which has the address of last node
	temp = last;
	while (temp->prev != last)			//Itrates the loop till same last address will not came
	{
		printf("%d -> ", temp->data);
		temp = temp->prev;
	}
	printf("%d ", temp->data);
	printf("\n");
}

/* Detailed description of the function.
 * This function find particular node in the circular doubly linked list
 * @head is input which is the head node of linked list
 * @data is input from user 
 * @return : No Return value
 */
int find(nod *head, int data)
{
	nod *temp;
	temp = head;
	//Compare data with the last data
	if((head -> prev) -> data == data)
	{
		return 1;
	}
	//Compare data with the intermediate data
	while(temp -> next != head)
	{
		if(temp -> data == data)
		{
			return 1;
		}
		temp = temp -> next;
	}
	return 0;

}
int main()
{
	nod *head = NULL;
	int number_of_node = 0;            //Count Number of nodes
	int i = 0;			   //Used for itretion
	int data[100];			   //Array of data
	int position = 0;		   //The position for insert or delete the node 
	int new_data = 0;		   //Store the value of new_data for insert node function
	int status_flag = 0;	   	   //Weather the node is present or not in list
	printf("\nEnter the number of nodes:");
	scanf("%d",&number_of_node);
	printf("\nEnter the 1 node:");
	scanf("%d",&data[i]);
	insertBegin(&head, data[0]);
	for(i = 2 ; i < number_of_node ; i++)
	{
		printf("Enter the %d node:",i);
		scanf("%d",&data[i]);
		insertEnd(&head, data[i]);
	}
	printf("\nCircular doubly linked list is: ");
	print(head);
	Reverse_print(head);
	printf("\nFirst node is deleted");
	deleteatbegin(&head);
	print(head);
	printf("\nLast node is deleted");
	deleteatend(&head);
	print(head);
	printf("\nEnter at which position you want to delete the data:");
	scanf("%d", &position);
	deleteatAfter(&head, position);
	print(head);
	printf("\nEnter at which position you want to add the data:");
	scanf("%d", &position);
	printf("\nEnter the data:");
	scanf("%d", &new_data);
	insertAfter(&head, position, new_data);
	print(head);
	printf("\nEnter the data to be found:");
	scanf("%d",&new_data);
	status_flag = find(head, new_data);
	if(status_flag == 1)
	{
		printf("\n%d data is found in list\n",new_data);
	}
	else
	{
		printf("\n%d data is not found in list\n",new_data);
	}
	return 0;
}
